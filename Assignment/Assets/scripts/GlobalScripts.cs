﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalScripts {

    public static List<GameObject> listOfSprites = new List<GameObject>(); //creating public arrayLists to be used in other scripts
    public static List<GameObject> usedSprites = new List<GameObject>();
    public static List<GameObject> listOfSpritesAgain = new List<GameObject>();
    public static List<GameObject> usedSpritesAgain = new List<GameObject>();

    public static bool check = false;

    public static void ShowJoker()
    {
        GameObject[] myJokers = GameObject.FindGameObjectsWithTag("joker"); //finding the back card tagged joker
        foreach(GameObject temp in myJokers)
        {
            temp.GetComponent<SpriteRenderer>().enabled = true;
        }
    }


}
