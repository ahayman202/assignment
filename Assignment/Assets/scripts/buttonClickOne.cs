﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class buttonClickOne : MonoBehaviour {
    public Button yourButton; //creating a public button later used as the start button

	// Use this for initialization
	void Start () {
        Button Start = yourButton.GetComponent<Button>();
        Start.onClick.AddListener(taskOnClick); //giving the action when button is clicked
	}
	
	// Update is called once per frame
	void taskOnClick () {
        SceneManager.LoadScene("game"); //game scene is loaded when start button is clicked
	}

  
}
