﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class buttonQuit : MonoBehaviour
{
    public Button myButton;

    // Use this for initialization
    void Start()
    {
        Button btnExit = myButton.GetComponent<Button>();
        btnExit.onClick.AddListener(taskOnClickable);
    }

    // Update is called once per frame
    void taskOnClickable()
    {
        Application.Quit();
    }
}