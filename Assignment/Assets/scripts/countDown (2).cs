﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class countDown : MonoBehaviour {
	float timeLeft = 300.0f;
	public Text text;
	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		timeLeft -= Time.deltaTime;
		text.text = "Time left : " + Mathf.Round (timeLeft);
		if (timeLeft < 0) {
			Application.LoadLevel ("gameOver");
		}
	}
}
