﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class countDown : MonoBehaviour {
	float timeLeft = 60.0f;
    bool stop = true;
	public Text text;
    float minutes;
    float seconds;
	// Use this for initialization
    void startTimer(float from)
    {
        stop = false;
        timeLeft = from;
        Update();
        StartCoroutine(updateCoroutine());
    }
	
	// Update is called once per frame
	void Update () {
        if (stop) return;
		timeLeft -= Time.deltaTime;

        minutes=Mathf.Floor(timeLeft / 60);
        seconds = timeLeft % 60;
        if (seconds > 59) seconds = 59;

		if (minutes < 0) {
            stop = true;
            minutes = 0;
            seconds = 0;


			SceneManager.LoadScene ("gameOver");
		}
	}
    private IEnumerator updateCoroutine()
    {
        while (!stop)
        {
            text.text = string.Format("{0:0}:{1:0}", minutes, seconds);
            yield return new WaitForSeconds(0.2f);
        }
    }
}
