﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class onClick : MonoBehaviour {
	public Button yourButton;
	// Use this for initialization
	void Start () {
		Button btn = yourButton.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
	}
	void TaskOnClick(){
		Debug.Log ("You have clicked the button!");
	}
	// Update is called once per frame
	void Update () {
	
	}
}
