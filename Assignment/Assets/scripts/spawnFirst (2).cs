﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class spawnFirst : MonoBehaviour
{

    public GameObject backOfCard;


    float x, y;



    Vector3 startPosition;



    // Use this for initialization
    void Start()
    {
        GlobalScripts.check = false;
        GlobalScripts.listOfSprites.AddRange(Resources.LoadAll<GameObject>("prefabs"));
        while (GlobalScripts.check == false)
        {
            firstEighteen();
        }

    }


    // Update is called once per frame
    void Update()
    {
        

    }


    void firstEighteen()
    {

        int location = Random.Range(0, GlobalScripts.listOfSprites.Count);
        GameObject tempObject = GlobalScripts.listOfSprites[location];

        if (GlobalScripts.usedSprites.Contains(tempObject) == false)
        {
            GlobalScripts.usedSprites.Add(tempObject);


            x = transform.position.x;
            y = transform.position.y;



            transform.position = new Vector3(x, y, 0);
            GameObject.Instantiate(tempObject, transform.position, transform.rotation);

            GameObject.Instantiate(backOfCard, transform.position, transform.rotation);

            GlobalScripts.check = true;





        }
       
    }


}
