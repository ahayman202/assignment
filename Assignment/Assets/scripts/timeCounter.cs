using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class timeCounter : MonoBehaviour {
	public Text text; //creating public text
	float startTime = 120.0f; //number of seconds
	// Use this for initialization
	void Start () {
		
	
	}
	
	// Update is called once per frame
	void Update () {
		startTime -= Time.deltaTime;
		text.text = "Time Left: " + Mathf.Round(startTime); //convert text into the counter

		if (startTime < 0) {
			SceneManager.LoadScene ("gameOver"); //load gameOver when counter reaches 0

		}

	}

}
