﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t477570861;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalScripts
struct  GlobalScripts_t591010005  : public Il2CppObject
{
public:

public:
};

struct GlobalScripts_t591010005_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GlobalScripts::listOfSprites
	List_1_t477570861 * ___listOfSprites_0;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GlobalScripts::usedSprites
	List_1_t477570861 * ___usedSprites_1;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GlobalScripts::listOfSpritesAgain
	List_1_t477570861 * ___listOfSpritesAgain_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GlobalScripts::usedSpritesAgain
	List_1_t477570861 * ___usedSpritesAgain_3;
	// System.Boolean GlobalScripts::check
	bool ___check_4;

public:
	inline static int32_t get_offset_of_listOfSprites_0() { return static_cast<int32_t>(offsetof(GlobalScripts_t591010005_StaticFields, ___listOfSprites_0)); }
	inline List_1_t477570861 * get_listOfSprites_0() const { return ___listOfSprites_0; }
	inline List_1_t477570861 ** get_address_of_listOfSprites_0() { return &___listOfSprites_0; }
	inline void set_listOfSprites_0(List_1_t477570861 * value)
	{
		___listOfSprites_0 = value;
		Il2CppCodeGenWriteBarrier(&___listOfSprites_0, value);
	}

	inline static int32_t get_offset_of_usedSprites_1() { return static_cast<int32_t>(offsetof(GlobalScripts_t591010005_StaticFields, ___usedSprites_1)); }
	inline List_1_t477570861 * get_usedSprites_1() const { return ___usedSprites_1; }
	inline List_1_t477570861 ** get_address_of_usedSprites_1() { return &___usedSprites_1; }
	inline void set_usedSprites_1(List_1_t477570861 * value)
	{
		___usedSprites_1 = value;
		Il2CppCodeGenWriteBarrier(&___usedSprites_1, value);
	}

	inline static int32_t get_offset_of_listOfSpritesAgain_2() { return static_cast<int32_t>(offsetof(GlobalScripts_t591010005_StaticFields, ___listOfSpritesAgain_2)); }
	inline List_1_t477570861 * get_listOfSpritesAgain_2() const { return ___listOfSpritesAgain_2; }
	inline List_1_t477570861 ** get_address_of_listOfSpritesAgain_2() { return &___listOfSpritesAgain_2; }
	inline void set_listOfSpritesAgain_2(List_1_t477570861 * value)
	{
		___listOfSpritesAgain_2 = value;
		Il2CppCodeGenWriteBarrier(&___listOfSpritesAgain_2, value);
	}

	inline static int32_t get_offset_of_usedSpritesAgain_3() { return static_cast<int32_t>(offsetof(GlobalScripts_t591010005_StaticFields, ___usedSpritesAgain_3)); }
	inline List_1_t477570861 * get_usedSpritesAgain_3() const { return ___usedSpritesAgain_3; }
	inline List_1_t477570861 ** get_address_of_usedSpritesAgain_3() { return &___usedSpritesAgain_3; }
	inline void set_usedSpritesAgain_3(List_1_t477570861 * value)
	{
		___usedSpritesAgain_3 = value;
		Il2CppCodeGenWriteBarrier(&___usedSpritesAgain_3, value);
	}

	inline static int32_t get_offset_of_check_4() { return static_cast<int32_t>(offsetof(GlobalScripts_t591010005_StaticFields, ___check_4)); }
	inline bool get_check_4() const { return ___check_4; }
	inline bool* get_address_of_check_4() { return &___check_4; }
	inline void set_check_4(bool value)
	{
		___check_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
