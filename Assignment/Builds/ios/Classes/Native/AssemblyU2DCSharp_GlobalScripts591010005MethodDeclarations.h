﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalScripts
struct GlobalScripts_t591010005;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalScripts::.ctor()
extern "C"  void GlobalScripts__ctor_m2389784552 (GlobalScripts_t591010005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalScripts::.cctor()
extern "C"  void GlobalScripts__cctor_m4053212807 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalScripts::ShowJoker()
extern "C"  void GlobalScripts_ShowJoker_m2487697820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
