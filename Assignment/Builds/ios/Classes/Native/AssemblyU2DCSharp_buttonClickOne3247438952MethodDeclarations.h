﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// buttonClickOne
struct buttonClickOne_t3247438952;

#include "codegen/il2cpp-codegen.h"

// System.Void buttonClickOne::.ctor()
extern "C"  void buttonClickOne__ctor_m3283276511 (buttonClickOne_t3247438952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void buttonClickOne::Start()
extern "C"  void buttonClickOne_Start_m3488407315 (buttonClickOne_t3247438952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void buttonClickOne::taskOnClick()
extern "C"  void buttonClickOne_taskOnClick_m3887607517 (buttonClickOne_t3247438952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
