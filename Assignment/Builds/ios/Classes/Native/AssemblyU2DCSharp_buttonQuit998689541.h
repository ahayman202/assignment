﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2491204935;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonQuit
struct  buttonQuit_t998689541  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.UI.Button buttonQuit::myButton
	Button_t2491204935 * ___myButton_2;

public:
	inline static int32_t get_offset_of_myButton_2() { return static_cast<int32_t>(offsetof(buttonQuit_t998689541, ___myButton_2)); }
	inline Button_t2491204935 * get_myButton_2() const { return ___myButton_2; }
	inline Button_t2491204935 ** get_address_of_myButton_2() { return &___myButton_2; }
	inline void set_myButton_2(Button_t2491204935 * value)
	{
		___myButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___myButton_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
