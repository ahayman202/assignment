﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// buttonQuit
struct buttonQuit_t998689541;

#include "codegen/il2cpp-codegen.h"

// System.Void buttonQuit::.ctor()
extern "C"  void buttonQuit__ctor_m3836838938 (buttonQuit_t998689541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void buttonQuit::Start()
extern "C"  void buttonQuit_Start_m2645057362 (buttonQuit_t998689541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void buttonQuit::taskOnClickable()
extern "C"  void buttonQuit_taskOnClickable_m3787981690 (buttonQuit_t998689541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
