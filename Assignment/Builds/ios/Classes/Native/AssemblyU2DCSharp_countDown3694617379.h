﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3921196294;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// countDown
struct  countDown_t3694617379  : public MonoBehaviour_t774292115
{
public:
	// System.Single countDown::timeLeft
	float ___timeLeft_2;
	// System.Boolean countDown::stop
	bool ___stop_3;
	// UnityEngine.UI.Text countDown::text
	Text_t3921196294 * ___text_4;
	// System.Single countDown::minutes
	float ___minutes_5;
	// System.Single countDown::seconds
	float ___seconds_6;

public:
	inline static int32_t get_offset_of_timeLeft_2() { return static_cast<int32_t>(offsetof(countDown_t3694617379, ___timeLeft_2)); }
	inline float get_timeLeft_2() const { return ___timeLeft_2; }
	inline float* get_address_of_timeLeft_2() { return &___timeLeft_2; }
	inline void set_timeLeft_2(float value)
	{
		___timeLeft_2 = value;
	}

	inline static int32_t get_offset_of_stop_3() { return static_cast<int32_t>(offsetof(countDown_t3694617379, ___stop_3)); }
	inline bool get_stop_3() const { return ___stop_3; }
	inline bool* get_address_of_stop_3() { return &___stop_3; }
	inline void set_stop_3(bool value)
	{
		___stop_3 = value;
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(countDown_t3694617379, ___text_4)); }
	inline Text_t3921196294 * get_text_4() const { return ___text_4; }
	inline Text_t3921196294 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t3921196294 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}

	inline static int32_t get_offset_of_minutes_5() { return static_cast<int32_t>(offsetof(countDown_t3694617379, ___minutes_5)); }
	inline float get_minutes_5() const { return ___minutes_5; }
	inline float* get_address_of_minutes_5() { return &___minutes_5; }
	inline void set_minutes_5(float value)
	{
		___minutes_5 = value;
	}

	inline static int32_t get_offset_of_seconds_6() { return static_cast<int32_t>(offsetof(countDown_t3694617379, ___seconds_6)); }
	inline float get_seconds_6() const { return ___seconds_6; }
	inline float* get_address_of_seconds_6() { return &___seconds_6; }
	inline void set_seconds_6(float value)
	{
		___seconds_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
