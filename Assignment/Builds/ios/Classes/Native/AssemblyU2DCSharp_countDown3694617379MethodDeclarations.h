﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// countDown
struct countDown_t3694617379;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;

#include "codegen/il2cpp-codegen.h"

// System.Void countDown::.ctor()
extern "C"  void countDown__ctor_m3544514030 (countDown_t3694617379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void countDown::startTimer(System.Single)
extern "C"  void countDown_startTimer_m1455769674 (countDown_t3694617379 * __this, float ___from0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void countDown::Update()
extern "C"  void countDown_Update_m2344490819 (countDown_t3694617379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator countDown::updateCoroutine()
extern "C"  Il2CppObject * countDown_updateCoroutine_m582901231 (countDown_t3694617379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
