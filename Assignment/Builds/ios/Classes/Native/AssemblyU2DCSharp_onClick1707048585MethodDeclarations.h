﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// onClick
struct onClick_t1707048585;

#include "codegen/il2cpp-codegen.h"

// System.Void onClick::.ctor()
extern "C"  void onClick__ctor_m3188771182 (onClick_t1707048585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void onClick::Start()
extern "C"  void onClick_Start_m2933753998 (onClick_t1707048585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void onClick::TaskOnClick()
extern "C"  void onClick_TaskOnClick_m2200417422 (onClick_t1707048585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void onClick::Update()
extern "C"  void onClick_Update_m722122801 (onClick_t1707048585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
