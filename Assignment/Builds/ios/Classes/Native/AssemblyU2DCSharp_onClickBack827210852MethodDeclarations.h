﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// onClickBack
struct onClickBack_t827210852;

#include "codegen/il2cpp-codegen.h"

// System.Void onClickBack::.ctor()
extern "C"  void onClickBack__ctor_m1181939379 (onClickBack_t827210852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void onClickBack::Start()
extern "C"  void onClickBack_Start_m3069675839 (onClickBack_t827210852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void onClickBack::Update()
extern "C"  void onClickBack_Update_m4153204070 (onClickBack_t827210852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void onClickBack::OnMouseDown()
extern "C"  void onClickBack_OnMouseDown_m3448044485 (onClickBack_t827210852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
