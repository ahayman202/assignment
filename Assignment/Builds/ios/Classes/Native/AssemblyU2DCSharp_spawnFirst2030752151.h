﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "UnityEngine_UnityEngine_Vector3465617797.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// spawnFirst
struct  spawnFirst_t2030752151  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GameObject spawnFirst::backOfCard
	GameObject_t1366199518 * ___backOfCard_2;
	// System.Single spawnFirst::x
	float ___x_3;
	// System.Single spawnFirst::y
	float ___y_4;
	// UnityEngine.Vector3 spawnFirst::startPosition
	Vector3_t465617797  ___startPosition_5;

public:
	inline static int32_t get_offset_of_backOfCard_2() { return static_cast<int32_t>(offsetof(spawnFirst_t2030752151, ___backOfCard_2)); }
	inline GameObject_t1366199518 * get_backOfCard_2() const { return ___backOfCard_2; }
	inline GameObject_t1366199518 ** get_address_of_backOfCard_2() { return &___backOfCard_2; }
	inline void set_backOfCard_2(GameObject_t1366199518 * value)
	{
		___backOfCard_2 = value;
		Il2CppCodeGenWriteBarrier(&___backOfCard_2, value);
	}

	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(spawnFirst_t2030752151, ___x_3)); }
	inline float get_x_3() const { return ___x_3; }
	inline float* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(float value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(spawnFirst_t2030752151, ___y_4)); }
	inline float get_y_4() const { return ___y_4; }
	inline float* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(float value)
	{
		___y_4 = value;
	}

	inline static int32_t get_offset_of_startPosition_5() { return static_cast<int32_t>(offsetof(spawnFirst_t2030752151, ___startPosition_5)); }
	inline Vector3_t465617797  get_startPosition_5() const { return ___startPosition_5; }
	inline Vector3_t465617797 * get_address_of_startPosition_5() { return &___startPosition_5; }
	inline void set_startPosition_5(Vector3_t465617797  value)
	{
		___startPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
