﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// spawnFirst
struct spawnFirst_t2030752151;

#include "codegen/il2cpp-codegen.h"

// System.Void spawnFirst::.ctor()
extern "C"  void spawnFirst__ctor_m3447149790 (spawnFirst_t2030752151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void spawnFirst::Start()
extern "C"  void spawnFirst_Start_m3493458882 (spawnFirst_t2030752151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void spawnFirst::Update()
extern "C"  void spawnFirst_Update_m861190911 (spawnFirst_t2030752151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void spawnFirst::firstEighteen()
extern "C"  void spawnFirst_firstEighteen_m3967951161 (spawnFirst_t2030752151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void spawnFirst::OnMouseDown()
extern "C"  void spawnFirst_OnMouseDown_m3345775066 (spawnFirst_t2030752151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
