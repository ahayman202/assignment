﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// spawnSecond
struct spawnSecond_t2766599053;

#include "codegen/il2cpp-codegen.h"

// System.Void spawnSecond::.ctor()
extern "C"  void spawnSecond__ctor_m1668183794 (spawnSecond_t2766599053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void spawnSecond::Start()
extern "C"  void spawnSecond_Start_m106806162 (spawnSecond_t2766599053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void spawnSecond::Update()
extern "C"  void spawnSecond_Update_m2591119317 (spawnSecond_t2766599053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void spawnSecond::secondEighteen()
extern "C"  void spawnSecond_secondEighteen_m3789440829 (spawnSecond_t2766599053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
