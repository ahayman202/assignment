﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3921196294;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// timeCounter
struct  timeCounter_t3639818647  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.UI.Text timeCounter::text
	Text_t3921196294 * ___text_2;
	// System.Single timeCounter::startTime
	float ___startTime_3;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(timeCounter_t3639818647, ___text_2)); }
	inline Text_t3921196294 * get_text_2() const { return ___text_2; }
	inline Text_t3921196294 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t3921196294 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_startTime_3() { return static_cast<int32_t>(offsetof(timeCounter_t3639818647, ___startTime_3)); }
	inline float get_startTime_3() const { return ___startTime_3; }
	inline float* get_address_of_startTime_3() { return &___startTime_3; }
	inline void set_startTime_3(float value)
	{
		___startTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
