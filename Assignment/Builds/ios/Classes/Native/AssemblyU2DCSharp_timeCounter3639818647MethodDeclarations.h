﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// timeCounter
struct timeCounter_t3639818647;

#include "codegen/il2cpp-codegen.h"

// System.Void timeCounter::.ctor()
extern "C"  void timeCounter__ctor_m415219526 (timeCounter_t3639818647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void timeCounter::Start()
extern "C"  void timeCounter_Start_m3957052658 (timeCounter_t3639818647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void timeCounter::Update()
extern "C"  void timeCounter_Update_m2733667963 (timeCounter_t3639818647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
