﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// buttonClickOne
struct buttonClickOne_t3247438952;
// UnityEngine.UI.Button
struct Button_t2491204935;
// System.Object
struct Il2CppObject;
// buttonQuit
struct buttonQuit_t998689541;
// countDown
struct countDown_t3694617379;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;
// countDown/<updateCoroutine>c__Iterator0
struct U3CupdateCoroutineU3Ec__Iterator0_t1616895666;
// GlobalScripts
struct GlobalScripts_t591010005;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3863917503;
// onClick
struct onClick_t1707048585;
// onClickBack
struct onClickBack_t827210852;
// spawnFirst
struct spawnFirst_t2030752151;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2005073387;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3632007997;
// spawnSecond
struct spawnSecond_t2766599053;
// timeCounter
struct timeCounter_t3639818647;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array4136897760.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_buttonClickOne3247438952.h"
#include "AssemblyU2DCSharp_buttonClickOne3247438952MethodDeclarations.h"
#include "mscorlib_System_Void2799814243.h"
#include "UnityEngine_UnityEngine_MonoBehaviour774292115MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component1078601330MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2491204935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction625099497MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent789719291MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2491204935.h"
#include "UnityEngine_UnityEngine_Component1078601330.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked3753894607.h"
#include "UnityEngine_UnityEngine_Events_UnityAction625099497.h"
#include "mscorlib_System_Object707969140.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManage834387985MethodDeclarations.h"
#include "mscorlib_System_String1967731336.h"
#include "AssemblyU2DCSharp_buttonQuit998689541.h"
#include "AssemblyU2DCSharp_buttonQuit998689541MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application3439453659MethodDeclarations.h"
#include "AssemblyU2DCSharp_countDown3694617379.h"
#include "AssemblyU2DCSharp_countDown3694617379MethodDeclarations.h"
#include "mscorlib_System_Single1791520093.h"
#include "mscorlib_System_Boolean3143194569.h"
#include "UnityEngine_UnityEngine_Coroutine3261918659.h"
#include "UnityEngine_UnityEngine_Time2587606660MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1692945841MethodDeclarations.h"
#include "AssemblyU2DCSharp_countDown_U3CupdateCoroutineU3Ec1616895666MethodDeclarations.h"
#include "AssemblyU2DCSharp_countDown_U3CupdateCoroutineU3Ec1616895666.h"
#include "mscorlib_System_Object707969140MethodDeclarations.h"
#include "mscorlib_System_String1967731336MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1717981302MethodDeclarations.h"
#include "mscorlib_System_UInt323922122178.h"
#include "mscorlib_System_Int321448170597.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3921196294.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3921196294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1717981302.h"
#include "mscorlib_System_NotSupportedException3178859535MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException3178859535.h"
#include "AssemblyU2DCSharp_GlobalScripts591010005.h"
#include "AssemblyU2DCSharp_GlobalScripts591010005MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen477570861MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen477570861.h"
#include "UnityEngine_UnityEngine_GameObject1366199518MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer2715231144MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"
#include "UnityEngine_UnityEngine_SpriteRenderer3863917503.h"
#include "AssemblyU2DCSharp_onClick1707048585.h"
#include "AssemblyU2DCSharp_onClick1707048585MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug12548584MethodDeclarations.h"
#include "AssemblyU2DCSharp_onClickBack827210852.h"
#include "AssemblyU2DCSharp_onClickBack827210852MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1181371020MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1181371020.h"
#include "AssemblyU2DCSharp_spawnFirst2030752151.h"
#include "AssemblyU2DCSharp_spawnFirst2030752151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources3616872916MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources3616872916.h"
#include "UnityEngine_UnityEngine_Random265084658MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform224878301MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3465617797MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3465617797.h"
#include "UnityEngine_UnityEngine_Transform224878301.h"
#include "UnityEngine_UnityEngine_Quaternion83606849.h"
#include "AssemblyU2DCSharp_spawnSecond2766599053.h"
#include "AssemblyU2DCSharp_spawnSecond2766599053MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat738568981MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat738568981.h"
#include "AssemblyU2DCSharp_timeCounter3639818647.h"
#include "AssemblyU2DCSharp_timeCounter3639818647MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t1078601330 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2491204935_m3412601438(__this, method) ((  Button_t2491204935 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1366199518 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631(__this, method) ((  SpriteRenderer_t3863917503 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0[] UnityEngine.Resources::LoadAll<System.Object>(System.String)
extern "C"  ObjectU5BU5D_t3632007997* Resources_LoadAll_TisIl2CppObject_m579936881_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_LoadAll_TisIl2CppObject_m579936881(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3632007997* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_LoadAll_TisIl2CppObject_m579936881_gshared)(__this /* static, unused */, p0, method)
// !!0[] UnityEngine.Resources::LoadAll<UnityEngine.GameObject>(System.String)
#define Resources_LoadAll_TisGameObject_t1366199518_m1920528223(__this /* static, unused */, p0, method) ((  GameObjectU5BU5D_t2005073387* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_LoadAll_TisIl2CppObject_m579936881_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void buttonClickOne::.ctor()
extern "C"  void buttonClickOne__ctor_m3283276511 (buttonClickOne_t3247438952 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonClickOne::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2491204935_m3412601438_MethodInfo_var;
extern const MethodInfo* buttonClickOne_taskOnClick_m3887607517_MethodInfo_var;
extern const uint32_t buttonClickOne_Start_m3488407315_MetadataUsageId;
extern "C"  void buttonClickOne_Start_m3488407315 (buttonClickOne_t3247438952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (buttonClickOne_Start_m3488407315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	{
		Button_t2491204935 * L_0 = __this->get_yourButton_2();
		NullCheck(L_0);
		Button_t2491204935 * L_1 = Component_GetComponent_TisButton_t2491204935_m3412601438(L_0, /*hidden argument*/Component_GetComponent_TisButton_t2491204935_m3412601438_MethodInfo_var);
		V_0 = L_1;
		Button_t2491204935 * L_2 = V_0;
		NullCheck(L_2);
		ButtonClickedEvent_t3753894607 * L_3 = Button_get_onClick_m1595880935(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)buttonClickOne_taskOnClick_m3887607517_MethodInfo_var);
		UnityAction_t625099497 * L_5 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityEvent_AddListener_m1596810379(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonClickOne::taskOnClick()
extern Il2CppCodeGenString* _stringLiteral2328218740;
extern const uint32_t buttonClickOne_taskOnClick_m3887607517_MetadataUsageId;
extern "C"  void buttonClickOne_taskOnClick_m3887607517 (buttonClickOne_t3247438952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (buttonClickOne_taskOnClick_m3887607517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2328218740, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonQuit::.ctor()
extern "C"  void buttonQuit__ctor_m3836838938 (buttonQuit_t998689541 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonQuit::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2491204935_m3412601438_MethodInfo_var;
extern const MethodInfo* buttonQuit_taskOnClickable_m3787981690_MethodInfo_var;
extern const uint32_t buttonQuit_Start_m2645057362_MetadataUsageId;
extern "C"  void buttonQuit_Start_m2645057362 (buttonQuit_t998689541 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (buttonQuit_Start_m2645057362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	{
		Button_t2491204935 * L_0 = __this->get_myButton_2();
		NullCheck(L_0);
		Button_t2491204935 * L_1 = Component_GetComponent_TisButton_t2491204935_m3412601438(L_0, /*hidden argument*/Component_GetComponent_TisButton_t2491204935_m3412601438_MethodInfo_var);
		V_0 = L_1;
		Button_t2491204935 * L_2 = V_0;
		NullCheck(L_2);
		ButtonClickedEvent_t3753894607 * L_3 = Button_get_onClick_m1595880935(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)buttonQuit_taskOnClickable_m3787981690_MethodInfo_var);
		UnityAction_t625099497 * L_5 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityEvent_AddListener_m1596810379(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonQuit::taskOnClickable()
extern "C"  void buttonQuit_taskOnClickable_m3787981690 (buttonQuit_t998689541 * __this, const MethodInfo* method)
{
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void countDown::.ctor()
extern "C"  void countDown__ctor_m3544514030 (countDown_t3694617379 * __this, const MethodInfo* method)
{
	{
		__this->set_timeLeft_2((60.0f));
		__this->set_stop_3((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void countDown::startTimer(System.Single)
extern "C"  void countDown_startTimer_m1455769674 (countDown_t3694617379 * __this, float ___from0, const MethodInfo* method)
{
	{
		__this->set_stop_3((bool)0);
		float L_0 = ___from0;
		__this->set_timeLeft_2(L_0);
		countDown_Update_m2344490819(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = countDown_updateCoroutine_m582901231(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void countDown::Update()
extern Il2CppClass* Mathf_t1692945841_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1883338814;
extern const uint32_t countDown_Update_m2344490819_MetadataUsageId;
extern "C"  void countDown_Update_m2344490819 (countDown_t3694617379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (countDown_Update_m2344490819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_stop_3();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = __this->get_timeLeft_2();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeLeft_2(((float)((float)L_1-(float)L_2)));
		float L_3 = __this->get_timeLeft_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1692945841_il2cpp_TypeInfo_var);
		float L_4 = floorf(((float)((float)L_3/(float)(60.0f))));
		__this->set_minutes_5(L_4);
		float L_5 = __this->get_timeLeft_2();
		__this->set_seconds_6((fmodf(L_5, (60.0f))));
		float L_6 = __this->get_seconds_6();
		if ((!(((float)L_6) > ((float)(59.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		__this->set_seconds_6((59.0f));
	}

IL_0062:
	{
		float L_7 = __this->get_minutes_5();
		if ((!(((float)L_7) < ((float)(0.0f)))))
		{
			goto IL_0099;
		}
	}
	{
		__this->set_stop_3((bool)1);
		__this->set_minutes_5((0.0f));
		__this->set_seconds_6((0.0f));
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1883338814, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Collections.IEnumerator countDown::updateCoroutine()
extern Il2CppClass* U3CupdateCoroutineU3Ec__Iterator0_t1616895666_il2cpp_TypeInfo_var;
extern const uint32_t countDown_updateCoroutine_m582901231_MetadataUsageId;
extern "C"  Il2CppObject * countDown_updateCoroutine_m582901231 (countDown_t3694617379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (countDown_updateCoroutine_m582901231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * V_0 = NULL;
	{
		U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * L_0 = (U3CupdateCoroutineU3Ec__Iterator0_t1616895666 *)il2cpp_codegen_object_new(U3CupdateCoroutineU3Ec__Iterator0_t1616895666_il2cpp_TypeInfo_var);
		U3CupdateCoroutineU3Ec__Iterator0__ctor_m3445311265(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * L_2 = V_0;
		return L_2;
	}
}
// System.Void countDown/<updateCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CupdateCoroutineU3Ec__Iterator0__ctor_m3445311265 (U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object countDown/<updateCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CupdateCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2145476751 (U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object countDown/<updateCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CupdateCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2237404775 (U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean countDown/<updateCoroutine>c__Iterator0::MoveNext()
extern Il2CppClass* Single_t1791520093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355566203;
extern const uint32_t U3CupdateCoroutineU3Ec__Iterator0_MoveNext_m2580067755_MetadataUsageId;
extern "C"  bool U3CupdateCoroutineU3Ec__Iterator0_MoveNext_m2580067755 (U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CupdateCoroutineU3Ec__Iterator0_MoveNext_m2580067755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0093;
	}

IL_0021:
	{
		goto IL_007c;
	}

IL_0026:
	{
		countDown_t3694617379 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Text_t3921196294 * L_3 = L_2->get_text_4();
		countDown_t3694617379 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		float L_5 = L_4->get_minutes_5();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t1791520093_il2cpp_TypeInfo_var, &L_6);
		countDown_t3694617379 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		float L_9 = L_8->get_seconds_6();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t1791520093_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3355566203, L_7, L_11, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_12);
		WaitForSeconds_t1717981302 * L_13 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_13, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_13);
		__this->set_U24PC_0(1);
		goto IL_0095;
	}

IL_007c:
	{
		countDown_t3694617379 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		bool L_15 = L_14->get_stop_3();
		if (!L_15)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_0093:
	{
		return (bool)0;
	}

IL_0095:
	{
		return (bool)1;
	}
	// Dead block : IL_0097: ldloc.1
}
// System.Void countDown/<updateCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CupdateCoroutineU3Ec__Iterator0_Dispose_m1890827840 (U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void countDown/<updateCoroutine>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CupdateCoroutineU3Ec__Iterator0_Reset_m3275554750_MetadataUsageId;
extern "C"  void U3CupdateCoroutineU3Ec__Iterator0_Reset_m3275554750 (U3CupdateCoroutineU3Ec__Iterator0_t1616895666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CupdateCoroutineU3Ec__Iterator0_Reset_m3275554750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GlobalScripts::.ctor()
extern "C"  void GlobalScripts__ctor_m2389784552 (GlobalScripts_t591010005 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlobalScripts::.cctor()
extern Il2CppClass* List_1_t477570861_il2cpp_TypeInfo_var;
extern Il2CppClass* GlobalScripts_t591010005_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t GlobalScripts__cctor_m4053212807_MetadataUsageId;
extern "C"  void GlobalScripts__cctor_m4053212807 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalScripts__cctor_m4053212807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t477570861 * L_0 = (List_1_t477570861 *)il2cpp_codegen_object_new(List_1_t477570861_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_listOfSprites_0(L_0);
		List_1_t477570861 * L_1 = (List_1_t477570861 *)il2cpp_codegen_object_new(List_1_t477570861_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_1, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_usedSprites_1(L_1);
		List_1_t477570861 * L_2 = (List_1_t477570861 *)il2cpp_codegen_object_new(List_1_t477570861_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_2, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_listOfSpritesAgain_2(L_2);
		List_1_t477570861 * L_3 = (List_1_t477570861 *)il2cpp_codegen_object_new(List_1_t477570861_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_3, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_usedSpritesAgain_3(L_3);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_check_4((bool)0);
		return;
	}
}
// System.Void GlobalScripts::ShowJoker()
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1601807361;
extern const uint32_t GlobalScripts_ShowJoker_m2487697820_MetadataUsageId;
extern "C"  void GlobalScripts_ShowJoker_m2487697820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalScripts_ShowJoker_m2487697820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t2005073387* V_0 = NULL;
	GameObject_t1366199518 * V_1 = NULL;
	GameObjectU5BU5D_t2005073387* V_2 = NULL;
	int32_t V_3 = 0;
	{
		GameObjectU5BU5D_t2005073387* L_0 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral1601807361, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObjectU5BU5D_t2005073387* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_0028;
	}

IL_0014:
	{
		GameObjectU5BU5D_t2005073387* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		GameObject_t1366199518 * L_5 = V_1;
		NullCheck(L_5);
		SpriteRenderer_t3863917503 * L_6 = GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631(L_5, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631_MethodInfo_var);
		NullCheck(L_6);
		Renderer_set_enabled_m142717579(L_6, (bool)1, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_8 = V_3;
		GameObjectU5BU5D_t2005073387* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void onClick::.ctor()
extern "C"  void onClick__ctor_m3188771182 (onClick_t1707048585 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void onClick::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2491204935_m3412601438_MethodInfo_var;
extern const MethodInfo* onClick_TaskOnClick_m2200417422_MethodInfo_var;
extern const uint32_t onClick_Start_m2933753998_MetadataUsageId;
extern "C"  void onClick_Start_m2933753998 (onClick_t1707048585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (onClick_Start_m2933753998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	{
		Button_t2491204935 * L_0 = __this->get_yourButton_2();
		NullCheck(L_0);
		Button_t2491204935 * L_1 = Component_GetComponent_TisButton_t2491204935_m3412601438(L_0, /*hidden argument*/Component_GetComponent_TisButton_t2491204935_m3412601438_MethodInfo_var);
		V_0 = L_1;
		Button_t2491204935 * L_2 = V_0;
		NullCheck(L_2);
		ButtonClickedEvent_t3753894607 * L_3 = Button_get_onClick_m1595880935(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)onClick_TaskOnClick_m2200417422_MethodInfo_var);
		UnityAction_t625099497 * L_5 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityEvent_AddListener_m1596810379(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void onClick::TaskOnClick()
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral101283044;
extern const uint32_t onClick_TaskOnClick_m2200417422_MetadataUsageId;
extern "C"  void onClick_TaskOnClick_m2200417422 (onClick_t1707048585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (onClick_TaskOnClick_m2200417422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral101283044, /*hidden argument*/NULL);
		return;
	}
}
// System.Void onClick::Update()
extern "C"  void onClick_Update_m722122801 (onClick_t1707048585 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void onClickBack::.ctor()
extern "C"  void onClickBack__ctor_m1181939379 (onClickBack_t827210852 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void onClickBack::Start()
extern "C"  void onClickBack_Start_m3069675839 (onClickBack_t827210852 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void onClickBack::Update()
extern "C"  void onClickBack_Update_m4153204070 (onClickBack_t827210852 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void onClickBack::OnMouseDown()
extern "C"  void onClickBack_OnMouseDown_m3448044485 (onClickBack_t827210852 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void spawnFirst::.ctor()
extern "C"  void spawnFirst__ctor_m3447149790 (spawnFirst_t2030752151 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void spawnFirst::Start()
extern Il2CppClass* GlobalScripts_t591010005_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_LoadAll_TisGameObject_t1366199518_m1920528223_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1026490093_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2864004577;
extern const uint32_t spawnFirst_Start_m3493458882_MetadataUsageId;
extern "C"  void spawnFirst_Start_m3493458882 (spawnFirst_t2030752151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (spawnFirst_Start_m3493458882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_check_4((bool)0);
		List_1_t477570861 * L_0 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_listOfSprites_0();
		GameObjectU5BU5D_t2005073387* L_1 = Resources_LoadAll_TisGameObject_t1366199518_m1920528223(NULL /*static, unused*/, _stringLiteral2864004577, /*hidden argument*/Resources_LoadAll_TisGameObject_t1366199518_m1920528223_MethodInfo_var);
		NullCheck(L_0);
		List_1_AddRange_m1026490093(L_0, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/List_1_AddRange_m1026490093_MethodInfo_var);
		goto IL_0025;
	}

IL_001f:
	{
		spawnFirst_firstEighteen_m3967951161(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		bool L_2 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_check_4();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}
}
// System.Void spawnFirst::Update()
extern "C"  void spawnFirst_Update_m861190911 (spawnFirst_t2030752151 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void spawnFirst::firstEighteen()
extern Il2CppClass* GlobalScripts_t591010005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern const uint32_t spawnFirst_firstEighteen_m3967951161_MetadataUsageId;
extern "C"  void spawnFirst_firstEighteen_m3967951161 (spawnFirst_t2030752151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (spawnFirst_firstEighteen_m3967951161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1366199518 * V_1 = NULL;
	Vector3_t465617797  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t465617797  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		List_1_t477570861 * L_0 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_listOfSprites_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count() */, L_0);
		int32_t L_2 = Random_Range_m694320887(NULL /*static, unused*/, 0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		List_1_t477570861 * L_3 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_listOfSprites_0();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		GameObject_t1366199518 * L_5 = VirtFuncInvoker1< GameObject_t1366199518 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32) */, L_3, L_4);
		V_1 = L_5;
		List_1_t477570861 * L_6 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_usedSprites_1();
		GameObject_t1366199518 * L_7 = V_1;
		NullCheck(L_6);
		bool L_8 = VirtFuncInvoker1< bool, GameObject_t1366199518 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Contains(!0) */, L_6, L_7);
		if (L_8)
		{
			goto IL_00e0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		List_1_t477570861 * L_9 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_usedSprites_1();
		GameObject_t1366199518 * L_10 = V_1;
		NullCheck(L_9);
		VirtActionInvoker1< GameObject_t1366199518 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0) */, L_9, L_10);
		Transform_t224878301 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t465617797  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = (&V_2)->get_x_1();
		__this->set_x_3(L_13);
		Transform_t224878301 * L_14 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t465617797  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_y_2();
		__this->set_y_4(L_16);
		Transform_t224878301 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_18 = __this->get_x_3();
		float L_19 = __this->get_y_4();
		Vector3_t465617797  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2638739322(&L_20, L_18, L_19, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m2469242620(L_17, L_20, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_21 = V_1;
		Transform_t224878301 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t465617797  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		Transform_t224878301 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Quaternion_t83606849  L_25 = Transform_get_rotation_m1033555130(L_24, /*hidden argument*/NULL);
		Object_Instantiate_m938141395(NULL /*static, unused*/, L_21, L_23, L_25, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_26 = __this->get_backOfCard_2();
		Transform_t224878301 * L_27 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t465617797  L_28 = Transform_get_position_m1104419803(L_27, /*hidden argument*/NULL);
		Transform_t224878301 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Quaternion_t83606849  L_30 = Transform_get_rotation_m1033555130(L_29, /*hidden argument*/NULL);
		Object_Instantiate_m938141395(NULL /*static, unused*/, L_26, L_28, L_30, /*hidden argument*/NULL);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_check_4((bool)1);
		GameObject_t1366199518 * L_31 = __this->get_backOfCard_2();
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m2079638459(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		return;
	}
}
// System.Void spawnFirst::OnMouseDown()
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern const uint32_t spawnFirst_OnMouseDown_m3345775066_MetadataUsageId;
extern "C"  void spawnFirst_OnMouseDown_m3345775066 (spawnFirst_t2030752151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (spawnFirst_OnMouseDown_m3345775066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = __this->get_backOfCard_2();
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2079638459(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void spawnSecond::.ctor()
extern "C"  void spawnSecond__ctor_m1668183794 (spawnSecond_t2766599053 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void spawnSecond::Start()
extern Il2CppClass* GlobalScripts_t591010005_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_LoadAll_TisGameObject_t1366199518_m1920528223_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1026490093_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2864004577;
extern const uint32_t spawnSecond_Start_m106806162_MetadataUsageId;
extern "C"  void spawnSecond_Start_m106806162 (spawnSecond_t2766599053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (spawnSecond_Start_m106806162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_check_4((bool)0);
		List_1_t477570861 * L_0 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_listOfSpritesAgain_2();
		GameObjectU5BU5D_t2005073387* L_1 = Resources_LoadAll_TisGameObject_t1366199518_m1920528223(NULL /*static, unused*/, _stringLiteral2864004577, /*hidden argument*/Resources_LoadAll_TisGameObject_t1366199518_m1920528223_MethodInfo_var);
		NullCheck(L_0);
		List_1_AddRange_m1026490093(L_0, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/List_1_AddRange_m1026490093_MethodInfo_var);
		goto IL_0025;
	}

IL_001f:
	{
		spawnSecond_secondEighteen_m3789440829(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		bool L_2 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_check_4();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}
}
// System.Void spawnSecond::Update()
extern "C"  void spawnSecond_Update_m2591119317 (spawnSecond_t2766599053 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void spawnSecond::secondEighteen()
extern Il2CppClass* GlobalScripts_t591010005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t738568981_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1786818228_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const uint32_t spawnSecond_secondEighteen_m3789440829_MetadataUsageId;
extern "C"  void spawnSecond_secondEighteen_m3789440829 (spawnSecond_t2766599053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (spawnSecond_secondEighteen_m3789440829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1366199518 * V_0 = NULL;
	Enumerator_t738568981  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	GameObject_t1366199518 * V_3 = NULL;
	Vector3_t465617797  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t465617797  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1145979430 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1145979430 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		List_1_t477570861 * L_0 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_listOfSpritesAgain_2();
		NullCheck(L_0);
		Enumerator_t738568981  L_1 = List_1_GetEnumerator_m970439620(L_0, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0023;
		}

IL_0010:
		{
			GameObject_t1366199518 * L_2 = Enumerator_get_Current_m2389888842((&V_1), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_0 = L_2;
			GameObject_t1366199518 * L_3 = V_0;
			NullCheck(L_3);
			String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0023:
		{
			bool L_5 = Enumerator_MoveNext_m3635305532((&V_1), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_5)
			{
				goto IL_0010;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1145979430 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		Enumerator_t738568981  L_6 = V_1;
		Enumerator_t738568981  L_7 = L_6;
		Il2CppObject * L_8 = Box(Enumerator_t738568981_il2cpp_TypeInfo_var, &L_7);
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1786818228_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
		IL2CPP_END_FINALLY(52)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1145979430 *)
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		List_1_t477570861 * L_9 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_listOfSpritesAgain_2();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count() */, L_9);
		int32_t L_11 = Random_Range_m694320887(NULL /*static, unused*/, 0, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		List_1_t477570861 * L_12 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_listOfSpritesAgain_2();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		GameObject_t1366199518 * L_14 = VirtFuncInvoker1< GameObject_t1366199518 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32) */, L_12, L_13);
		V_3 = L_14;
		List_1_t477570861 * L_15 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_usedSpritesAgain_3();
		GameObject_t1366199518 * L_16 = V_3;
		NullCheck(L_15);
		bool L_17 = VirtFuncInvoker1< bool, GameObject_t1366199518 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Contains(!0) */, L_15, L_16);
		if (L_17)
		{
			goto IL_0112;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalScripts_t591010005_il2cpp_TypeInfo_var);
		List_1_t477570861 * L_18 = ((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->get_usedSpritesAgain_3();
		GameObject_t1366199518 * L_19 = V_3;
		NullCheck(L_18);
		VirtActionInvoker1< GameObject_t1366199518 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0) */, L_18, L_19);
		Transform_t224878301 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t465617797  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		float L_22 = (&V_4)->get_x_1();
		__this->set_x_3(L_22);
		Transform_t224878301 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t465617797  L_24 = Transform_get_position_m1104419803(L_23, /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = (&V_5)->get_y_2();
		__this->set_y_4(L_25);
		Transform_t224878301 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_27 = __this->get_x_3();
		float L_28 = __this->get_y_4();
		Vector3_t465617797  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector3__ctor_m2638739322(&L_29, L_27, L_28, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m2469242620(L_26, L_29, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_30 = V_3;
		Transform_t224878301 * L_31 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t465617797  L_32 = Transform_get_position_m1104419803(L_31, /*hidden argument*/NULL);
		Transform_t224878301 * L_33 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Quaternion_t83606849  L_34 = Transform_get_rotation_m1033555130(L_33, /*hidden argument*/NULL);
		Object_Instantiate_m938141395(NULL /*static, unused*/, L_30, L_32, L_34, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_35 = __this->get_backOfCard_2();
		Transform_t224878301 * L_36 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t465617797  L_37 = Transform_get_position_m1104419803(L_36, /*hidden argument*/NULL);
		Transform_t224878301 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		Quaternion_t83606849  L_39 = Transform_get_rotation_m1033555130(L_38, /*hidden argument*/NULL);
		Object_Instantiate_m938141395(NULL /*static, unused*/, L_35, L_37, L_39, /*hidden argument*/NULL);
		((GlobalScripts_t591010005_StaticFields*)GlobalScripts_t591010005_il2cpp_TypeInfo_var->static_fields)->set_check_4((bool)1);
	}

IL_0112:
	{
		return;
	}
}
// System.Void timeCounter::.ctor()
extern "C"  void timeCounter__ctor_m415219526 (timeCounter_t3639818647 * __this, const MethodInfo* method)
{
	{
		__this->set_startTime_3((120.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void timeCounter::Start()
extern "C"  void timeCounter_Start_m3957052658 (timeCounter_t3639818647 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void timeCounter::Update()
extern Il2CppClass* Mathf_t1692945841_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t1791520093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral729860554;
extern Il2CppCodeGenString* _stringLiteral1883338814;
extern const uint32_t timeCounter_Update_m2733667963_MetadataUsageId;
extern "C"  void timeCounter_Update_m2733667963 (timeCounter_t3639818647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (timeCounter_Update_m2733667963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_startTime_3();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_3(((float)((float)L_0-(float)L_1)));
		Text_t3921196294 * L_2 = __this->get_text_2();
		float L_3 = __this->get_startTime_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1692945841_il2cpp_TypeInfo_var);
		float L_4 = bankers_roundf(L_3);
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(Single_t1791520093_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral729860554, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_7);
		float L_8 = __this->get_startTime_3();
		if ((!(((float)L_8) < ((float)(0.0f)))))
		{
			goto IL_0051;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1883338814, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
